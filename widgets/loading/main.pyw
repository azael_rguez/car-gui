# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'loading/untitled.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(640, 480)

        self.loadingGifLabel = QtWidgets.QLabel(Form)
        self.loadingGifLabel.setGeometry(QtCore.QRect(200, 80, 300, 300))
        self.loadingGifLabel.setText("")
        self.loadingGifLabel.setScaledContents(True)
        self.loadingGifLabel.setObjectName("label")
        
        self.movie = QMovie("loading.gif", QByteArray()) 
        self.movie.setCacheMode(QMovie.CacheAll) 
        self.movie.setSpeed(100) 
        self.loadingGifLabel.setMovie(self.movie) 
        self.movie.start()
        #self.movie.stop()

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

