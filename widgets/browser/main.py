""" import sys

from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWebEngineWidgets import QWebEngineView

app = QApplication(sys.argv)
wv = QWebEngineView()
wv.load(QUrl("https://www.google.com"))
wv.show()
app.exec_() """


import sys

from PyQt5 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets

app = QtWidgets.QApplication(sys.argv)
wv = QtWebEngineWidgets.QWebEngineView()
wv.load(QtCore.QUrl("https://www.google.com"))
wv.show()
app.exec_()