# Car GUI

Simple car GUI using Python and QT to install in a Rapberry pi with a touch screen.

## Getting Started

### Main dependencies

* PyQT5

### Main commands

Installing dependencies

```
pip install -r requirements.txt
```

Converting images and design to code

```
pyrcc5 design/img/img.qrc -o build/img_rc.py
pyuic5 -x design/ui-stable.ui -o build/main.pyw
```

Running the tests

```
cd build
python main.pyw
```

## Authors

### Original Author and Development Lead

**Azael Rodríguez** 
* [Twitter](https://twitter.com/azael_rguez) 
* [Gitlab](https://gitlab.com/azael_rguez)

## Licence

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

