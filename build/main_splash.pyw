# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'design/ui-alpha.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 480)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(10, 0))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons-main/icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet("QMainWindow {\n"
"    background-color:qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgb(0, 0, 0), stop:1 rgb(40, 40, 40));\n"
"}\n"
"\n"
"QPushButton {\n"
"    border: 5px solid rgba(0, 0, 0, 0);\n"
"    border-radius: 50px;\n"
"}\n"
"\n"
"/*phoneButton*/\n"
"QPushButton#phoneButton{\n"
"    background-color:qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(44, 196, 27, 255), stop:1 rgba(23, 96, 15, 255));\n"
"}\n"
"QPushButton#phoneButton:hover {\n"
"    background-color:rgba(44, 196, 27, 255);\n"
"}\n"
"QPushButton#phoneButton:pressed {\n"
"    background-color:rgba(23, 96, 15, 255);\n"
"}\n"
"\n"
"/*musicButton*/\n"
"QPushButton#musicButton{\n"
"    background-color:qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(66, 134, 244, 255), stop:1 rgba(31, 61, 109, 255));\n"
"}\n"
"QPushButton#musicButton:hover {\n"
"    background-color:rgba(66, 134, 244, 255);\n"
"}\n"
"QPushButton#musicButton:pressed {\n"
"    background-color:rgba(31, 61, 109, 255);\n"
"}\n"
"\n"
"/*mapButton*/\n"
"QPushButton#mapButton {\n"
"    background-color:qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(189, 63, 211, 255), stop:1 rgba(104, 34, 117, 255));\n"
"}\n"
"QPushButton#mapButton:hover {\n"
"    background-color: rgba(189, 63, 211, 255);\n"
"}\n"
"QPushButton#mapButton:pressed {\n"
"    background-color: rgba(104, 34, 117, 255);\n"
"}\n"
"\n"
"/*browserButton*/\n"
"QPushButton#browserButton {\n"
"    background-color:qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(223, 232, 53, 255), stop:1 rgba(97, 102, 11, 255));\n"
"}\n"
"QPushButton#browserButton:hover {\n"
"    background-color:rgba(223, 232, 53, 255);\n"
"}\n"
"QPushButton#browserButton:pressed {\n"
"    background-color:rgba(97, 102, 11, 255);\n"
"}\n"
"\n"
"/*terminalButton*/\n"
"QPushButton#terminalButton {\n"
"    background-color:qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(229, 120, 52, 255), stop:1 rgba(132, 60, 15, 255));\n"
"}\n"
"QPushButton#terminalButton:hover {\n"
"    background-color:rgba(229, 120, 52, 255);\n"
"}\n"
"QPushButton#terminalButton:pressed {\n"
"    background-color:rgba(132, 60, 15, 255);\n"
"}\n"
"\n"
"/*configButton*/\n"
"QPushButton#configButton {\n"
"    background-color:qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(86, 86, 86), stop:1 rgba(22, 22, 22, 255));\n"
"}\n"
"QPushButton#configButton:hover {\n"
"    background-color: rgba(86, 86, 86);\n"
"}\n"
"QPushButton#configButton:pressed    {\n"
"    background-color: rgba(22, 22, 22, 255);\n"
"}\n"
"\n"
"/*Labels*/\n"
"QLabel {\n"
"    color: rgb(255, 255, 255);\n"
"    font-size: 16px;\n"
"}\n"
"\n"
"QLabel#timeLabel {\n"
"    font-size: 22px;\n"
"    font-style: bold;\n"
"}\n"
"\n"
"QPushButton#leftButton, QPushButton#homeButton, QPushButton#rightButton {\n"
"    margin-top:20px;\n"
"}\n"
"\n"
"QGroupBox {\n"
"    border: 0px solid rgba(0, 0, 0, 0);\n"
"}\n"
"\n"
"QProgressBar {\n"
"    border: 1px solid rgba(0, 0, 0, 0);\n"
"    border-radius: 5px;\n"
"    padding: 1px;\n"
"    background-color: rgb(0, 0, 0);\n"
"    color: rgb(255, 255, 255);\n"
"}\n"
"\n"
" QProgressBar::chunk {\n"
"     background-color:rgb(66, 134, 244);\n"
"    border-radius: 5px;\n"
" }\n"
"")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.stackedWidget = QtWidgets.QStackedWidget(self.centralwidget)
        self.stackedWidget.setObjectName("stackedWidget")
        self.splashPage = QtWidgets.QWidget()
        self.splashPage.setObjectName("splashPage")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.splashPage)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.progressBar = QtWidgets.QProgressBar(self.splashPage)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setAlignment(QtCore.Qt.AlignCenter)
        self.progressBar.setTextVisible(False)
        self.progressBar.setInvertedAppearance(False)
        self.progressBar.setObjectName("progressBar")
        self.gridLayout_3.addWidget(self.progressBar, 2, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(100, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem, 2, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.splashPage)
        self.label.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignHCenter)
        self.label.setObjectName("label")
        self.gridLayout_3.addWidget(self.label, 0, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(100, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem1, 2, 2, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(20, 50, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.gridLayout_3.addItem(spacerItem2, 3, 1, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.gridLayout_3.addItem(spacerItem3, 1, 1, 1, 1)
        self.stackedWidget.addWidget(self.splashPage)
        self.home_page = QtWidgets.QWidget()
        self.home_page.setObjectName("home_page")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.home_page)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.terminalButton = QtWidgets.QPushButton(self.home_page)
        self.terminalButton.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.terminalButton.sizePolicy().hasHeightForWidth())
        self.terminalButton.setSizePolicy(sizePolicy)
        self.terminalButton.setAutoFillBackground(False)
        self.terminalButton.setStyleSheet("")
        self.terminalButton.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/icons-home/terminal-ico.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.terminalButton.setIcon(icon1)
        self.terminalButton.setIconSize(QtCore.QSize(80, 80))
        self.terminalButton.setObjectName("terminalButton")
        self.gridLayout_2.addWidget(self.terminalButton, 4, 3, 1, 1)
        self.musicButton = QtWidgets.QPushButton(self.home_page)
        self.musicButton.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.musicButton.sizePolicy().hasHeightForWidth())
        self.musicButton.setSizePolicy(sizePolicy)
        self.musicButton.setAutoFillBackground(False)
        self.musicButton.setStyleSheet("")
        self.musicButton.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/icons-home/music-ico.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.musicButton.setIcon(icon2)
        self.musicButton.setIconSize(QtCore.QSize(80, 80))
        self.musicButton.setObjectName("musicButton")
        self.gridLayout_2.addWidget(self.musicButton, 1, 3, 1, 1)
        self.mapButton = QtWidgets.QPushButton(self.home_page)
        self.mapButton.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mapButton.sizePolicy().hasHeightForWidth())
        self.mapButton.setSizePolicy(sizePolicy)
        self.mapButton.setAutoFillBackground(False)
        self.mapButton.setStyleSheet("")
        self.mapButton.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/icons-home/map-ico.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.mapButton.setIcon(icon3)
        self.mapButton.setIconSize(QtCore.QSize(80, 80))
        self.mapButton.setObjectName("mapButton")
        self.gridLayout_2.addWidget(self.mapButton, 1, 5, 1, 1)
        self.musicLabel = QtWidgets.QLabel(self.home_page)
        self.musicLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.musicLabel.setObjectName("musicLabel")
        self.gridLayout_2.addWidget(self.musicLabel, 2, 3, 1, 1)
        self.terminalLabel = QtWidgets.QLabel(self.home_page)
        self.terminalLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.terminalLabel.setObjectName("terminalLabel")
        self.gridLayout_2.addWidget(self.terminalLabel, 5, 3, 1, 1)
        self.phoneLabel = QtWidgets.QLabel(self.home_page)
        self.phoneLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.phoneLabel.setObjectName("phoneLabel")
        self.gridLayout_2.addWidget(self.phoneLabel, 2, 1, 1, 1)
        self.phoneButton = QtWidgets.QPushButton(self.home_page)
        self.phoneButton.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.phoneButton.sizePolicy().hasHeightForWidth())
        self.phoneButton.setSizePolicy(sizePolicy)
        self.phoneButton.setMinimumSize(QtCore.QSize(0, 0))
        self.phoneButton.setMouseTracking(False)
        self.phoneButton.setTabletTracking(False)
        self.phoneButton.setAutoFillBackground(False)
        self.phoneButton.setStyleSheet("")
        self.phoneButton.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/icons-home/phone-ico.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.phoneButton.setIcon(icon4)
        self.phoneButton.setIconSize(QtCore.QSize(80, 80))
        self.phoneButton.setCheckable(False)
        self.phoneButton.setObjectName("phoneButton")
        self.gridLayout_2.addWidget(self.phoneButton, 1, 1, 1, 1)
        self.broserLabel = QtWidgets.QLabel(self.home_page)
        self.broserLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.broserLabel.setObjectName("broserLabel")
        self.gridLayout_2.addWidget(self.broserLabel, 5, 1, 1, 1)
        self.configLabel = QtWidgets.QLabel(self.home_page)
        self.configLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.configLabel.setObjectName("configLabel")
        self.gridLayout_2.addWidget(self.configLabel, 5, 5, 1, 1)
        self.browserButton = QtWidgets.QPushButton(self.home_page)
        self.browserButton.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.browserButton.sizePolicy().hasHeightForWidth())
        self.browserButton.setSizePolicy(sizePolicy)
        self.browserButton.setAutoFillBackground(False)
        self.browserButton.setStyleSheet("")
        self.browserButton.setText("")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/icons-home/chrome-ico.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.browserButton.setIcon(icon5)
        self.browserButton.setIconSize(QtCore.QSize(80, 80))
        self.browserButton.setObjectName("browserButton")
        self.gridLayout_2.addWidget(self.browserButton, 4, 1, 1, 1)
        self.mapLabel = QtWidgets.QLabel(self.home_page)
        self.mapLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.mapLabel.setObjectName("mapLabel")
        self.gridLayout_2.addWidget(self.mapLabel, 2, 5, 1, 1)
        spacerItem4 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem4, 3, 3, 1, 1)
        self.configButton = QtWidgets.QPushButton(self.home_page)
        self.configButton.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.configButton.sizePolicy().hasHeightForWidth())
        self.configButton.setSizePolicy(sizePolicy)
        self.configButton.setAutoFillBackground(False)
        self.configButton.setStyleSheet("")
        self.configButton.setText("")
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/icons-home/cog-ico.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.configButton.setIcon(icon6)
        self.configButton.setIconSize(QtCore.QSize(80, 80))
        self.configButton.setObjectName("configButton")
        self.gridLayout_2.addWidget(self.configButton, 4, 5, 1, 1)
        spacerItem5 = QtWidgets.QSpacerItem(90, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem5, 1, 6, 1, 1)
        spacerItem6 = QtWidgets.QSpacerItem(90, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem6, 1, 0, 1, 1)
        spacerItem7 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem7, 1, 2, 1, 1)
        spacerItem8 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem8, 1, 4, 1, 1)
        self.stackedWidget.addWidget(self.home_page)
        self.phonePage = QtWidgets.QWidget()
        self.phonePage.setObjectName("phonePage")
        self.stackedWidget.addWidget(self.phonePage)
        self.verticalLayout.addWidget(self.stackedWidget)
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setTitle("")
        self.groupBox.setFlat(True)
        self.groupBox.setCheckable(False)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.leftButton = QtWidgets.QPushButton(self.groupBox)
        self.leftButton.setText("")
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(":/icons-bar/left-ico.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.leftButton.setIcon(icon7)
        self.leftButton.setIconSize(QtCore.QSize(24, 24))
        self.leftButton.setObjectName("leftButton")
        self.horizontalLayout.addWidget(self.leftButton)
        self.homeButton = QtWidgets.QPushButton(self.groupBox)
        self.homeButton.setText("")
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap(":/icons-bar/home-ico.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.homeButton.setIcon(icon8)
        self.homeButton.setIconSize(QtCore.QSize(24, 24))
        self.homeButton.setObjectName("homeButton")
        self.horizontalLayout.addWidget(self.homeButton)
        self.rightButton = QtWidgets.QPushButton(self.groupBox)
        self.rightButton.setText("")
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap(":/icons-bar/right-ico.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.rightButton.setIcon(icon9)
        self.rightButton.setIconSize(QtCore.QSize(24, 24))
        self.rightButton.setObjectName("rightButton")
        self.horizontalLayout.addWidget(self.rightButton)
        self.verticalLayout.addWidget(self.groupBox)
        MainWindow.setCentralWidget(self.centralwidget)
        QtCore.QTimer.singleShot(3000, self.loadingContent)
        self.retranslateUi(MainWindow)
        self.stackedWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "OpsAuto"))
        self.label.setText(_translate("MainWindow", "Loading... Wait a moment"))
        self.musicLabel.setText(_translate("MainWindow", "Music"))
        self.terminalLabel.setText(_translate("MainWindow", "Terminal"))
        self.phoneLabel.setText(_translate("MainWindow", "Phone"))
        self.broserLabel.setText(_translate("MainWindow", "Internet browser"))
        self.configLabel.setText(_translate("MainWindow", "Configuration"))
        self.mapLabel.setText(_translate("MainWindow", "Maps"))


    def loadingContent(self):
        self.value = 0
        while self.value < 100:
            self.value += 0.0001
            self.progressBar.setValue(self.value)


import img_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

